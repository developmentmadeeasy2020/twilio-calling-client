# Basic Calling Functionality Configuration

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.This is a frontend code for Calling and its backend can be found [ here ] ( https://developmentmadeeasy2020@bitbucket.org/developmentmadeeasy2020/twilio-calling-server.git )

### Prerequisites

  Basic Knowledge with react, redux and sagas
  Twilio Puchased and Configured Number ( Say A = 2345678901 ) and a US number ( Say B = 7893654213 ) to have two way testing

# Configuration
    1) git checkout feature/calling

    2) Follow instructions at feature/calling branch's Readme.md for basic call functionality setup

