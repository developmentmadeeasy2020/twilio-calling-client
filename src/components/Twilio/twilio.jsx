import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import { connect } from 'react-redux'
// import loginstate from '../../utils/loginState'
// import config from '../../config'
import {
    getToken,
    disconnectCall,
    incomingCall,
    connectSocket,
} from './action'
import {
    ready,
    error,
    disconnect,
    incoming,
    cancel,
    offline,
    connect as twilioDeviceConnect,
} from '../../utils/actions/socketEvents'

// const TwilioDevice = props => {

// }

class TwilioDevice extends Component {
    componentDidMount() {
        const {
            props: { getTwilioToken, getSocketConnection, socketConnection },
        } = this
        // const userId = loginstate.userSession && loginstate.userSession.id
        getTwilioToken()
        // if (!socketConnection) getSocketConnection(`${config.SOCKET_BASE_URL}?userId=${userId}`)
    }

    componentDidUpdate(preProps) {
        const {
            props: { getSocketConnection, socketConnection, twilioDevice }, handleTwilioEvents,
        } = this
        const userId = loginstate.userSession && loginstate.userSession.id
        if ((preProps.twilioDevice === null && twilioDevice)
            || (preProps.twilioDevice && preProps.twilioDevice.token !== twilioDevice.token)) {
            handleTwilioEvents()
        }
        if (!socketConnection) getSocketConnection(`${config.SOCKET_BASE_URL}?userId=${userId}`)
    }

    componentWillUnmount() {
        const { unsubscribeTwilioEventListener } = this
        unsubscribeTwilioEventListener()
    }

    unsubscribeTwilioEventListener = () => {
        const { props: { twilioDevice } } = this
        twilioDevice.removeListener(ready, (key) => { })
        twilioDevice.removeListener(error, (key) => { })
        twilioDevice.removeListener(disconnect, (key) => { })
        twilioDevice.removeListener(offline, (key) => { })
        twilioDevice.removeListener(incoming, (key) => { })
        twilioDevice.removeListener(twilioDeviceConnect, (key) => { })
        twilioDevice.removeListener(cancel, (key) => { })
    }

    handleTwilioEvents = () => {
        const { props, unsubscribeTwilioEventListener } = this
        const {
            getTwilioToken,
            twilioDevice,
            endCall,
        } = props
        if (twilioDevice) {
            unsubscribeTwilioEventListener()
            twilioDevice.on(ready, (key) => { })

            twilioDevice.on(error, (err) => {
                if (err.code === 31205) {
                    getTwilioToken()
                }
            })
            twilioDevice.on(disconnect, (conn) => {
                endCall()
            })
            twilioDevice.on(offline, (device) => {
                console.log('OFFLINE')
            })
            twilioDevice.on(incoming, (conn) => {
                console.log('INCOMING')
            })
            twilioDevice.on(twilioDeviceConnect, (conn) => {
                console.log('DEVICE CONNECT')
            })
            twilioDevice.on(cancel, (conn) => {
                console.log('CANCEL')
            })
        }
    }

    render() {
        return (
            <div>
                Twilio Device  Div
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    getTwilioToken: () => dispatch(getToken()),
    endCall: () => dispatch(disconnectCall()),
    callIncoming: conn => dispatch(incomingCall(conn)),
    getSocketConnection: url => dispatch(connectSocket(url)),
})

const mapStateToProps = state => ({
    twilioDevice: state.twilioReducer.twilioDevice,
    activeConnection: state.twilioReducer.activeConnection,
    socketConnection: state.twilioReducer.socketConnection,
    otherConnections: state.twilioReducer.otherConnections,
    calling: state.dialerReducer.calling,
})

TwilioDevice.defaultProps = {
    twilioDevice: null,
    socketConnection: null,
    handleMessengerOpen: null,
    otherConnections: null,
}

TwilioDevice.propTypes = {
    getTwilioToken: PropTypes.func.isRequired,
    twilioDevice: PropTypes.objectOf(PropTypes.any),
    otherConnections: PropTypes.objectOf(PropTypes.any),
    endCall: PropTypes.func.isRequired,
    callIncoming: PropTypes.func.isRequired,
    getSocketConnection: PropTypes.func.isRequired,
    socketConnection: PropTypes.objectOf(PropTypes.any),
    openOutboundMessageModal: PropTypes.bool.isRequired,
    handleMessengerOpen: PropTypes.func,
    setAvailability: PropTypes.func.isRequired,
    incomingMultiple: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(TwilioDevice)
