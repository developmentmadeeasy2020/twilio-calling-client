import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Calling from './components/Calling/calling'

const Root = ({ store }) => {
    return (
        <React.Fragment>
            <Provider store={store}>
                <Router>
                    <Switch>
                        <Route path="/" render={() => <Calling /> } />
                    </Switch>
                </Router>
            </Provider>
        </React.Fragment>
    )
}

Root.propTypes = {
    store: PropTypes.shape({}).isRequired,
}

export default Root
