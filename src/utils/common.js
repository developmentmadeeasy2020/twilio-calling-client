const twilioNumber = '+123456789' // Mention Your Twilio Number A Here
const personalNumber = '+987654321' //Mention an Active user number b, If you dont have it, get it from login in to TextNow
const url = 'http://localhost:12345?userId=1'

export {
    twilioNumber,
    personalNumber,
    url
}