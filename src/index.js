import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducers from './reducers'
import './index.css';
import Root from './root';
import sagas from './saga'
import * as serviceWorker from './serviceWorker';


const sagaMiddleware = createSagaMiddleware()
const store = createStore( rootReducers, applyMiddleware( sagaMiddleware ) )
sagaMiddleware.run( sagas )

ReactDOM.render( <Root store={ store } />, document.getElementById('root'));



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
